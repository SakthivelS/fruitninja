const gameElement = document.getElementById("game");
const scoreElement = document.getElementById("score");
const finalScore = document.getElementById("finalScore");
const gameOver = document.querySelector(".gameOver");
const restartBtn = document.getElementById("restartButton");
const audio = document.getElementById("sliceSound");
const end = document.getElementById("end");
const highScore = document.getElementById("highScore");
// const trials = document.getElementById("trials");
const w = gameElement.offsetWidth;
const h = gameElement.offsetHeight;

highScore.innerText = localStorage.getItem("fruitNinjaHighScore") || 0;

var game = new Phaser.Game(w, h, Phaser.AUTO, "game", {
    preload: preload,
    create: create,
    update: update,
    render: render,
});

function preload() {
    game.load.image("button", "start.png");
    game.load.image("good", "assets/watermelon.png");
    game.load.image("bad", "assets/bomb.png");
    game.load.image("parts", "assets/part.png");
    game.load.image("cursor", "assets/sword.cur");
}

var good_objects,
    bad_objects,
    slashes,
    line,
    score = 0,
    points = [],
    start = false,
    trialsLeft = 5;

var fireRate = 1000;
var nextFire = 0;

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.physics.arcade.gravity.y = 300;

    good_objects = createGroup(4, "good");
    bad_objects = createGroup(4, "bad");

    slashes = game.add.graphics(0, 0);

    //change cursor to sword
    game.canvas.style.cursor = "url(assets/sword.cur), pointer";

    //start button
    button = game.add.button(game.world.centerX - 15, 400, "button", startButtonClick);

    //emit sliced pieces after fruit is cut
    emitter = game.add.emitter(0, 0, 300);
    emitter.makeParticles("parts");
    emitter.gravity = 300;
    emitter.setYSpeed(-400, 400);
}

function createGroup(numItems, sprite) {
    var group = game.add.group();
    group.enableBody = true;
    group.physicsBodyType = Phaser.Physics.ARCADE;
    group.createMultiple(numItems, sprite);
    group.setAll("checkWorldBounds", true);
    group.setAll("outOfBoundsKill", true);
    return group;
}

function throwObject() {
    if (game.time.now > nextFire && good_objects.countDead() > 0 && bad_objects.countDead() > 0) {
        nextFire = game.time.now + fireRate;
        throwGoodObject();
        if (Math.random() > 0.5) {
            throwBadObject();
        }
    }
}

function throwGoodObject() {
    var obj = good_objects.getFirstDead();
    obj.reset(game.world.centerX + Math.random() * 100 - Math.random() * 100, 600);
    obj.anchor.setTo(0.5, 0.5);
    game.physics.arcade.moveToXY(obj, game.world.centerX, game.world.centerY, 530);
}

function throwBadObject() {
    var obj = bad_objects.getFirstDead();
    obj.reset(game.world.centerX + Math.random() * 100 - Math.random() * 100, 600);
    obj.anchor.setTo(0.5, 0.5);
    game.physics.arcade.moveToXY(obj, game.world.centerX, game.world.centerY, 530);
}

function update() {
    if (start) {
        throwObject();
    }
    points.push({
        x: game.input.x,
        y: game.input.y,
    });
    points = points.splice(points.length - 10, points.length);

    if (points.length < 1 || points[0].x == 0) {
        return;
    }

    slashes.clear();
    slashes.beginFill(0xffffff);
    slashes.alpha = 0.5;
    slashes.moveTo(points[0].x, points[0].y);
    for (var i = 1; i < points.length; i++) {
        slashes.lineTo(points[i].x, points[i].y);
    }
    slashes.endFill();

    for (var i = 1; i < points.length; i++) {
        line = new Phaser.Line(points[i].x, points[i].y, points[i - 1].x, points[i - 1].y);
        game.debug.geom(line);

        good_objects.forEachExists(checkIntersects);
        bad_objects.forEachExists(checkIntersects);
    }
    scoreElement.innerText = score;
    //addHearts();
}

var contactPoint = new Phaser.Point(0, 0);

function checkIntersects(fruit, callback) {
    var l1 = new Phaser.Line(fruit.body.right - fruit.width, fruit.body.bottom - fruit.height, fruit.body.right, fruit.body.bottom);
    var l2 = new Phaser.Line(fruit.body.right - fruit.width, fruit.body.bottom, fruit.body.right, fruit.body.bottom - fruit.height);
    l2.angle = 90;

    if (Phaser.Line.intersects(line, l1, true) || Phaser.Line.intersects(line, l2, true)) {
        contactPoint.x = game.input.x;
        contactPoint.y = game.input.y;
        var distance = Phaser.Point.distance(contactPoint, new Phaser.Point(fruit.x, fruit.y));
        if (Phaser.Point.distance(contactPoint, new Phaser.Point(fruit.x, fruit.y)) > 110) {
            return;
        }

        if (fruit.parent == good_objects) {
            killFruit(fruit);
        } else {
            resetScore();
        }
    }
}

function resetScore() {
    //Update final score
    finalScore.innerText = score;
    gameOver.classList.add("show");
    end.play();

    // If score is greater than highscore, update high score
    if (score > localStorage.getItem("fruitNinjaHighScore")) {
        localStorage.setItem("fruitNinjaHighScore", score);
    }

    good_objects.forEachExists(killFruit);
    bad_objects.forEachExists(killFruit);
    //pause game
    game.paused = true;
}

function render() {}

function killFruit(fruit) {
    emitter.x = fruit.x;
    emitter.y = fruit.y;
    emitter.start(true, 2000, null, 4);
    fruit.kill();
    points = [];
    score++;
    audio.play();
}

function startButtonClick() {
    start = true;
    button.visible = false;
}

// function addHearts() {
//     trials.innerHTML = "";
//     for (i = 0; i < trialsLeft; i++) {
//         let img = document.createElement("img");
//         img.src = "./assets/heart.png";
//         let att = document.createAttribute("class"); // Create a "class" attribute
//         att.value = "life"; // Set the value of the class attribute
//         img.setAttributeNode(att);
//         trials.appendChild(img);
//     }
// }

restartBtn.addEventListener("click", (e) => {
    window.location = "/";
});
